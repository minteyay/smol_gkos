# smol GKOS controller

A compact 6-button keyboard based on the [GKOS chording system](http://gkos.com) with a small blackberry-style trackball mouse.
I wanted a combined keyboard + mouse to plug into a single USB port (simpler for USB OTG, and less wires to wrangle around) for controlling single board computers like Raspberry Pis, media PCs etc. I also wanted to try if the GKOS layout would be practical for use in terminal environments as well as more general use.

![a mint green 3d printed wedge shaped controller with four pink mouse buttons on one side and six white keycapped mechanical switches in two columns on the other](doc/gkos.jpg)

***Please note that I don't recommend you build one of these as-is, as there are quite a few improvements I'd like to make in a second version in the future (detailed below). It is a usable device, but it's up here more as a resource and less a ready-to-go thing to build.***

### Hardware
- six Kailh Choc switches (I used brown, though any colour should work) + keycaps
- [blackberry-style Pimoroni trackball breakout board](https://shop.pimoroni.com/products/trackball-breakout)
- [Teensy 3.2 microcontroller](https://www.pjrc.com/store/teensy32.html)
- short countersunk M2 screws and nuts
- three 3mm LEDs + 51k resistors (will vary depending on the specific LEDs)
- M2x3.5x8mm heat set inserts
- 3D printed case parts **(see the case/ folder for documentation!)**

### Layout changes and additions
The keyboard layout follows [the official GKOS character set](http://gkos.com/gkos/gkos-charset.html) fairly closely, but has a few changes made for use with desktop computers.
- an additional function key layer (similar to how the symbol set works), activated with the chord ACDE (in terms of the six physical keys pressed on their own)
  - F1-F10 mapped to the same places as the number keys, F11 = L/@, F12 = M/½
  - print screen = delete
  - media keys
    - volume up + down = up and down arrows
    - volume mute/unmute = space
    - play/pause = backspace
    - previous + next track = left and right arrows
- meta/super/command/windows instead of insert, to allow for operating system specific keyboard shortcuts
- the previous/next word and left/right arrow keys are swapped around since it felt more natural to map the regular arrows to the middle
- the native keymap is empty as I wasn't sure what to put there (combinable accents like ¨ and ´ might make sense)

### Improvements to make
This was pretty much my first device made from scratch (case design included), so there's a few things I want to improve on and change for a second version at some point:
- *Wires take up space.* ***Wires take up space.*** I did leave some space in the case for wires, but I didn't consider how they would fold down when the case is closed, nor how much thickness solder joints add to an otherwise flat PCB. As is, the bottom plate bows out a bit with all the wires stuffed in there. Some sort of physical clips and channels in the case would make sense as well.
- I noticed early on in designing the case that the small tactile switches I used combined with a simple "sandwiched" key cap made it wobble quite a bit. This wobble also meant that the key would only actuate when pressed directly over the tactile switch, and pressing anywhere else would bottom out without an actual button press being registered. The bending piece used in the final design, similar to most mouse keys, solved this problem, but the thickness of the bending part could use some tweaking to make it feel lighter in use. An alternative to tactile switches could be miniature microswitches for a closer feel to most mice.
- The mouse movement isn't all that smooth. I had to do my own mouse acceleration in the device itself as the small trackball reports back fairly small movements and most operating systems don't go high enough with their mouse sensitivity for it to be usable. Doing faster movements to move from one side of a big display to the other are quite hard to do on a small ball too, so the acceleration is pretty aggressive to compensate. It *works*, but it's sort of jittery.
    - A possible alternative with a similarly small size could be a Thinkpad Trackpoint nub.
- I wasn't sure how many LED indicators would be needed, and three was chosen just for symmetry. Currently the first two are dedicated to shift/shift lock and symbol/symbol lock, with the third one lighting up when any of the other modifiers (ctrl, alt, meta) are pressed. It'd make it a bit clearer to have five LEDs so all of the modifiers have a dedicated indicator light. The lockable modifiers (shift and symbol) could also use two different colours to differentiate between the single-use and lock states.
- The heat set inserts are too close to the outer walls, and the heat warped the PLA plastic a good bit. Just tapping screws directly into the plastic seems to work well for the other parts, but if I did want to keep the added durability with thread inserts, they'd definitely need to either be shorter (less surface to heat up and hopefully less heat overall) or have thicker walls around them.
